﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeAPI.Controllers
{
    [ApiController, Route("[controller]")]
    public class CoffeeController : ControllerBase
    {
        private static readonly string[] Coffees = {
            "Flat White", "Long Black", "Latte", "Americano", "Cappuccino"
        };

        [HttpGet]
        public IActionResult Get() => Ok(Coffees[new Random().Next(Coffees.Length)]);
    }
}