﻿namespace TeaAPI.Controllers.TeaController

open System
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging

[<ApiController>]
[<Route("[controller]")>]
type TeaController (_logger : ILogger<TeaController>) =
    inherit ControllerBase()
    
    let Teas = [| "Green"; "Peppermint"; "Earl Grey"; "English Breakfast"; "Camomile" |]
    
    [<HttpGet>]
    member __.Get() =
        ActionResult<string>(Teas.[Random().Next(Teas.Length)])